<?php
declare( strict_types = 1 );

namespace App\GraphQL\Queries;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class Index extends Query {
	protected $attributes = [
		'name' => 'index' ,
		'description' => 'A query',
	];
	
	public function type (): Type {
		return Type::listOf(Type::string());
	}
	
	public function args (): array {
		return [];
	}
	
	public function resolve ( $root , array $args , $context , ResolveInfo $resolveInfo , Closure $getSelectFields ) {
		return true;
	}
}
