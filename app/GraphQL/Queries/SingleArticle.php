<?php
declare( strict_types = 1 );

namespace App\GraphQL\Queries;

use App\Models\Article;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class SingleArticle extends Query {
	protected $attributes = [
		'name' => 'singleArticle' ,
		'description' => 'return single article by id' ,
	];
	
	public function type (): Type {
		return GraphQL::type('Article');
	}
	
	public function args (): array {
		return [
			'id' => [
				'type' => Type::int() ,
			] ,
		];
	}
	
	public function resolve ( $root , array $args , $context , ResolveInfo $resolveInfo , Closure $getSelectFields ) {
		$article = Article::query()
						  ->find($args[ 'id' ]);
		if ( !$article ) {
			return new \Error("article is not found");
		}
		
		return $article;
	}
}
