<?php
declare( strict_types = 1 );

namespace App\GraphQL\Queries;

use App\Models\Comment;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;
use function Illuminate\Events\queueable;

class AllComment extends Query {
	protected $attributes = [
		'name' => 'allComment' ,
		'description' => 'return all comments' ,
	];
	
	public function type (): Type {
		return Type::listOf(GraphQL::type('Comment'));
	}
	
	public function args (): array {
		return [];
	}
	
	public function resolve ( $root , array $args , $context , ResolveInfo $resolveInfo , Closure $getSelectFields ) {
		$comments = Comment::query()
						   ->with([ 'user' ])
						   ->get();
		
		return $comments;
	}
}
