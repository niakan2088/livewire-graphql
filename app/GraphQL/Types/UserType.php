<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name' => 'User',
        'description' => 'creating user type'
    ];

    public function fields(): array
    {
        return [
				'id' => [
					'type' => Type::string(),
					'description' => 'user id'
				],
				'articles' => [
					'type' => Type::listOf(GraphQL::type('Article'))
				],
				'name' => [
					'type' => Type::string(),
					'description' => 'user name'
				],
				'email' => [
					'type' => Type::string(),
					'description' => 'user email'
				],
				'admin' => [
					'type' => Type::boolean(),
					'description' => 'is user admin'
				],
				'created_at' => [
					'type' => Type::string(),
					'description' => 'user created_at'
				]
        ];
    }
}
