<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CommentType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Comment',
        'description' => 'create comment type'
    ];

    public function fields(): array
    {
        return [
			'id' => [
				'type' => Type::int(),
				'description' => 'comment id'
			],
			'user' => [
				'type' => GraphQL::type('User')
			],
			'approved' => [
				'type' => Type::boolean()
			],
			'body' => [
				'type' => Type::string(),
				'description' => 'comment body'
			],
			'created_at' => [
				'type' => Type::string(),
				'description' => 'comment created_at'
			]
        ];
    }
}
