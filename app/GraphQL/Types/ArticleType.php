<?php
declare( strict_types = 1 );

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ArticleType extends GraphQLType {
	protected $attributes = [
		'name' => 'Article' ,
		'description' => 'create article type' ,
	];
	
	public function fields (): array {
		return [
			'id' => [
				'type' => Type::int() ,
				'description' => 'article id' ,
			] ,
			'user' => [
				'type' => GraphQL::type('User'),
			] ,
			'title' => [
				'type' => Type::string() ,
				'description' => 'article title' ,
			] ,
			'body' => [
				'type' => Type::string() ,
				'description' => 'article description' ,
			] ,
			'comments' => [
				'type' => Type::listOf(GraphQL::type('Comment')) ,
			],
		];
	}
	
	protected function resolveCommentsField ( $data ) {
		return $data->comments()
					->where('approved' , true)
					->get();
	}
}
