<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <title>Hello, world!</title>
    @livewireStyles
</head>
<body>
<div class="container">
    {{$slot}}

</div>
@livewireScripts
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://nightly.datatables.net/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.11.3/dataRender/ellipsis.js"></script>
</body>
<script>
    let data = {
        query: `query getArticles($page: Int!,$limit: Int!){
              articles(page: $page, limit: $limit) {
                data {
                  id,
                  title,
                  body,
                  user {
                    id,
                    name
                    email
                  }
                }
                total
              }
            }`,
        variables: {
            page: 1,
            limit: 20
        }
    };
    fetch('http://127.0.0.1:8000/graphql', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(res => res.json())
        .then( function (res) {
            console.log(res.data.articles.data)
            var table = $('#example').DataTable({
                "bAutoWidth": false,
                "processing": true,
                "data": res.data.articles.data,
                "columns": [
                    {"data": "id", "title": "Id"},
                    {"data": "title", "title": "Title"},
                    {
                        "data": "body",
                        "title": "Body",
                        "render": $.fn.dataTable.render.ellipsis(50, true)
                    },
                    {
                        "data": "user.name",
                        "title": "Name",
                        "orderable": false
                    },
                    {
                        "data": "user.email",
                        "title": "Email",
                        "orderable": false
                    }
                ],
                "order": [[1, "asc"]]
            })
        })
    .catch(err => console.log(err));
</script>
</html>

